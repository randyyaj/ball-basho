extends Node2D

onready var red_player = get_node('red_player')
onready var blue_player = get_node('blue_player')

onready var red_button = get_node('red_button')
onready var red_button_unpressed = preload("res://asset/art/red_button_up.png")
onready var red_button_pressed = preload("res://asset/art/red_button_down.png")

onready var blue_button = get_node('blue_button')
onready var blue_button_unpressed = preload("res://asset/art/blue_button_up.png")
onready var blue_button_pressed = preload("res://asset/art/blue_button_down.png")

onready var ball = get_node('ball')

onready var sample_player = get_node('SamplePlayer')
onready var stream_player = get_node('StreamPlayer')

onready var max_speed_label = get_node('max_speed_label')
onready var header_label = get_node('header_label')
onready var replay_button = get_node('replay_button')
onready var game_main_label = get_node('game_main_label')
onready var timer = get_node('Timer')

const BALL_INITIAL_SPEED = 200
const BALL_SPEED_MAX = 1000
const BALL_SPEED_RAMP_INTERVAL = 20

var ball_initial_position
var ball_speed = BALL_INITIAL_SPEED
var direction = Vector2(1.0, 0.0)
var serve_ball = false

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	ball_initial_position = ball.get_pos()
	stream_player.play()
	set_fixed_process(true)
	set_process_input(true)
	set_process(true)
	
func _process(delta):
	if (timer.get_time_left() > 1.5):
		header_label.set_text(str(3))
	elif (timer.get_time_left() > 1.0):
		header_label.set_text(str(2))
	elif (timer.get_time_left() > 0.5):
		header_label.set_text(str(1))
	elif (timer.get_time_left() < 0.3 and timer.get_time_left() != 0):
		header_label.set_text("start!")
		
	if (serve_ball):
		ball.move(direction * ball_speed * delta)
		max_speed_label.show()
		max_speed_label.set_text("BALL SPEED: " + str(ball_speed))
	#ball.set_pos(Vector2(ball.get_pos().x*delta, 0.0))

func _input(event):
	if event.is_action_pressed('ui_accept'):
		reset()
		
	if event.is_action_pressed('button_red'):
		red_button.set_texture(red_button_pressed)
		sample_player.play('click')
		red_player.get_node('AnimationPlayer').play('swing')
		red_player.get_node('red_player_body/CollisionShape2D').set_trigger(false)
	else:
		red_button.set_texture(red_button_unpressed)
		red_player.get_node('red_player_body/CollisionShape2D').set_trigger(true)
		
	if event.is_action_pressed('button_blue'):
		blue_button.set_texture(blue_button_pressed)
		sample_player.play('click')
		blue_player.get_node('AnimationPlayer').play('swing')
		blue_player.get_node('blue_player_body/CollisionShape2D').set_trigger(false)
	else:
		blue_button.set_texture(blue_button_unpressed)
		blue_player.get_node('blue_player_body/CollisionShape2D').set_trigger(true)

func _on_red_button_input_event( viewport, event, shape_idx ):
	if (event.type == InputEvent.MOUSE_BUTTON and event.button_index == BUTTON_LEFT and event.pressed):
		red_button.set_texture(red_button_pressed)
		sample_player.play('click')
		red_player.get_node('AnimationPlayer').play('swing')
		red_player.get_node('red_player_body/CollisionShape2D').set_trigger(false)
	else:
		red_button.set_texture(red_button_unpressed)
		red_player.get_node('red_player_body/CollisionShape2D').set_trigger(true)
		
func _on_blue_button_input_event( viewport, event, shape_idx ):
	if event.type == InputEvent.MOUSE_BUTTON and event.button_index == BUTTON_LEFT and event.pressed:
		blue_button.set_texture(blue_button_pressed)
		sample_player.play('click')
		blue_player.get_node('AnimationPlayer').play('swing')
		blue_player.get_node('blue_player_body/CollisionShape2D').set_trigger(false)
	else:
		blue_button.set_texture(blue_button_unpressed)
		blue_player.get_node('blue_player_body/CollisionShape2D').set_trigger(true)

func _on_Timer_timeout():
	header_label.hide()
	game_main_label.hide()
	serve_ball=true

func reset():
	serve_ball = false
	replay_button.hide()
	ball.set_pos(ball_initial_position)
	ball_speed = BALL_INITIAL_SPEED
	timer.set_one_shot(true)
	timer.start()
	var rand = randi()%2+1
	if (rand == 1):
		direction = Vector2(1.0,0.0)
	else:
		direction = Vector2(-1.0,0.0)
	header_label.show()
	timer.set_wait_time(2)
		
func _on_blue_death_wall_body_enter( body ):
	header_label.show()
	header_label.set_text("RED WINS!")
	serve_ball = false
	replay_button.show()
	
func _on_red_death_wall_body_enter( body ):
	header_label.show()
	header_label.set_text("BLUE WINS!")
	serve_ball = false
	replay_button.show()
	
func _on_replay_button_pressed():
	reset()
