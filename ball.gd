extends KinematicBody2D

onready var parent = get_node("../")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if is_colliding():  # colliding with Static, Kinematic, Rigid
		if (get_collider().get_name().match("blue_player_body")):
			parent.direction = -parent.direction
			self.set_pos(Vector2(self.get_pos().x,parent.ball_initial_position.y))
			if parent.ball_speed < parent.BALL_SPEED_MAX:
				parent.ball_speed += parent.BALL_SPEED_RAMP_INTERVAL
			
		if (get_collider().get_name().match("red_player_body")):
			parent.direction = -parent.direction
			self.set_pos(Vector2(self.get_pos().x,parent.ball_initial_position.y))
			if parent.ball_speed < parent.BALL_SPEED_MAX:
				parent.ball_speed += parent.BALL_SPEED_RAMP_INTERVAL