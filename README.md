# Ball Basho

> A game made for the godot game jam 2016 
> Theme: Two Buttons

### Controls
Click on the red or blue button to whack the ball
Click Z or X for keyboard
Can be single or multiplayer (if you have friends)

License MIT
This game is OpenSauce(open source) please feel free to savor the goodness and share it with the world! 
All recipes and ingredients for this game is licensed under MIT License.
Some Assets such as font and/or sound may be subject to copyright. But for the most part you should be okay.
